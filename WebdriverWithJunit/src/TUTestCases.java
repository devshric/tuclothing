
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class TUTestCases {
	public static By SEARCHTEXTBOX = By.cssSelector("#search");
	public static By SEARCHBUTTON = By.cssSelector(".searchButton");
	public static WebDriver driver;
	public static By STORELOCATOR = By.cssSelector("a[href='/store-finder']");
	public static By POSTCODEORTOWN = By.cssSelector("input[placeholder='Postcode or town']");
	public static By FINDSTOREBUTTON = By.cssSelector("button[class='ln-c-button ln-c-button--primary']");
	public static By REGISTERBUTTON = By.cssSelector("a[href='/login']");
	public static By EMAILADDRESS = By.cssSelector("#register_email");
	public static By FIRSTNAME = By.cssSelector("#register_firstName");
	public static By LASTNAME = By.cssSelector("#register_lastName");
	public static By NEWPASSWORD = By.cssSelector("#password");
	public static By CONFIRMPWD = By.cssSelector("#register_checkPwd");
	public static By SUBMITBUTTON = By.cssSelector("#submit-register");
	public static By LOGIN = By.cssSelector("a[href='/login']");
	public static By USERNAME = By.cssSelector("#j_username");
	public static By PASSWORD = By.cssSelector("#j_password");
	public static By LOGINBUTTON = By.cssSelector("#submit-login");
	public static By CHECKOUTBUTTON = By.cssSelector("a[data-testid='checkoutButton']");
	public static By CHECKOUTBUTTONBOTTOM = By.cssSelector("#basketButtonBottom");

	@Before
	public void start() {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\CEX\\Documents\\DevshriClasses\\Automation\\chromedriver.exe");

		driver = new ChromeDriver();

		driver.get("https://tuclothing.sainsburys.co.uk/");
		Assert.assertEquals("Womens, Mens, Kids & Baby Fashion  | Tu clothing", driver.getTitle());
	}

	@Test
	public void searchWithValidData() {

		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("jeans");
		driver.findElement(SEARCHBUTTON).click();
		Assert.assertEquals("Search results for: jeans | Tu clothing", driver.getTitle());
	}

	@Test
	public void searchWithInvalidData() {

		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("ghfjhgj");
		driver.findElement(SEARCHBUTTON).click();
		Assert.assertEquals("Sorry, no results for 'ghfjhgj", driver.findElement(By.cssSelector("h1")).getText());

	}

	@Test
	public void searchWithBlank() {

		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("");
		driver.findElement(SEARCHBUTTON).click();
		Assert.assertEquals("Please complete a product search",
				driver.findElement(By.cssSelector("span[alt'Please complete a product search']")).getText());
	}

	@Test
	public void specialCharacters() {

		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("$$$$$$");
		driver.findElement(SEARCHBUTTON).click();
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=%24%24%24%24%24%24",
				driver.getCurrentUrl());

	}

	@Test
	public void searchWithValidDeptName() {

		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("Men");
		driver.findElement(SEARCHBUTTON).click();
		Assert.assertEquals("Mens Clothing | Jeans, T-Shirts, Shirts | Tu Clothing", driver.getTitle());

	}

	@Test
	public void searchWithInvalidDeptName() {

		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("ghfjhgj");
		driver.findElement(SEARCHBUTTON).click();
		Assert.assertEquals("Sorry, no results for 'ghfjhgj'", driver.findElement(By.cssSelector("h1")).getText());

	}

	@Test
	public void searchWithValidProductCode() {

		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("136397490");
		driver.findElement(SEARCHBUTTON).click();
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=136397490", driver.getCurrentUrl());

	}

	@Test
	public void searchWithInvalidProductCode() {

		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("12345678");
		driver.findElement(SEARCHBUTTON).click();
		Assert.assertEquals("Sorry, no results for '12345678'", driver.findElement(By.cssSelector("h1")).getText());
	}
	

	@Test
	public void storeLocatorWithValidPostcode() {
		driver.findElement(STORELOCATOR).click();

		driver.findElement(POSTCODEORTOWN).clear();
		driver.findElement(POSTCODEORTOWN).sendKeys("NW9 9BP");
		driver.findElement(By.cssSelector("label[for='children']")).click();

		driver.findElement(FINDSTOREBUTTON).click();
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder", driver.getCurrentUrl());
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder?q=NW9+9BP&men=on&CSRFToken=6d30bd50-01e4-4849-9e60-dc416fcd0157", driver.getCurrentUrl());

	}

	@Test
	public void storeLocatorWithInvalidPostcode() {
		driver.findElement(STORELOCATOR).click();

		driver.findElement(POSTCODEORTOWN).clear();
		driver.findElement(POSTCODEORTOWN).sendKeys("NW 9BP");
		driver.findElement(By.cssSelector("label[for='children']")).click();

		driver.findElement(FINDSTOREBUTTON).click();
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder", driver.getCurrentUrl());
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder?q=NW9+9BP&men=on&CSRFToken=6d30bd50-01e4-4849-9e60-dc416fcd0157", driver.getCurrentUrl());
	}

	@Test
	public void storeLocatorWithValidTownname() {
		driver.findElement(STORELOCATOR).click();

		driver.findElement(POSTCODEORTOWN).clear();
		driver.findElement(POSTCODEORTOWN).sendKeys("Stanmore");
		driver.findElement(By.cssSelector("label[for='children']")).click();

		driver.findElement(FINDSTOREBUTTON).click();
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder", driver.getCurrentUrl());
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder?q=Stanmore&men=on&CSRFToken=73cff779-7e21-4cb4-bea9-8448f5566e65", driver.getCurrentUrl());

	}

	@Test
	public void storeLocatorWithInvalidTownname() {
		driver.findElement(STORELOCATOR).click();

		driver.findElement(POSTCODEORTOWN).clear();
		driver.findElement(POSTCODEORTOWN).sendKeys("ksdlksdjl");
		driver.findElement(By.cssSelector("label[for='children']")).click();

		driver.findElement(FINDSTOREBUTTON).click();
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder", driver.getCurrentUrl());
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder?q=NW9+9BP&men=on&CSRFToken=6d30bd50-01e4-4849-9e60-dc416fcd0157", driver.getCurrentUrl());

	}

	@Test
	public void storeLocatorWithValidBlank() {
		driver.findElement(STORELOCATOR).click();

		driver.findElement(POSTCODEORTOWN).clear();
		driver.findElement(POSTCODEORTOWN).sendKeys("");
		driver.findElement(By.cssSelector("label[for='children']")).click();

		driver.findElement(FINDSTOREBUTTON).click();
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder", driver.getCurrentUrl());
		

	}

	@Test
	public void storeLocatorWithValidPCClicknCollect() {
		driver.findElement(STORELOCATOR).click();

		driver.findElement(POSTCODEORTOWN).clear();
		driver.findElement(POSTCODEORTOWN).sendKeys("NW9 9BP");
		driver.findElement(By.cssSelector("label[for='children']")).click();
		driver.findElement(By.cssSelector("label[for='click']")).click();
		driver.findElement(FINDSTOREBUTTON).click();
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder", driver.getCurrentUrl());
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder?q=NW9+9BP&men=on&click=on&CSRFToken=73cff779-7e21-4cb4-bea9-8448f5566e65",driver.getCurrentUrl());
	}

	@Test
	public void storeLocatorWithInvalidPCClicknCollect() {
		driver.findElement(STORELOCATOR).click();

		driver.findElement(POSTCODEORTOWN).clear();
		driver.findElement(POSTCODEORTOWN).sendKeys("NW 9BP");
		driver.findElement(By.cssSelector("label[for='children']")).click();
		driver.findElement(By.cssSelector("label[for='click']")).click();
		driver.findElement(FINDSTOREBUTTON).click();
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder", driver.getCurrentUrl());
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder?q=NW+9BP&men=on&click=on&CSRFToken=73cff779-7e21-4cb4-bea9-8448f5566e65",driver.getCurrentUrl());
	}

	@Test
	public void storeLocatorWithValidTNClicknCollect() {
		driver.findElement(STORELOCATOR).click();

		driver.findElement(POSTCODEORTOWN).clear();
		driver.findElement(POSTCODEORTOWN).sendKeys("Stanmore");
		driver.findElement(By.cssSelector("label[for='children']")).click();
		driver.findElement(By.cssSelector("label[for='click']")).click();
		driver.findElement(FINDSTOREBUTTON).click();
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder", driver.getCurrentUrl());
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder?q=Stanmore&CSRFToken=73cff779-7e21-4cb4-bea9-8448f5566e65",driver.getCurrentUrl());
	}

	@Test
	public void storeLocatorWithInvalidTNClicknCollect() {
		driver.findElement(STORELOCATOR).click();

		driver.findElement(POSTCODEORTOWN).clear();
		driver.findElement(POSTCODEORTOWN).sendKeys("jhkjhkjh");
		driver.findElement(By.cssSelector("label[for='children']")).click();
		driver.findElement(By.cssSelector("label[for='click']")).click();
		driver.findElement(FINDSTOREBUTTON).click();
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder", driver.getCurrentUrl());
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder?q=jhkjhkjh&children=on&click=on&CSRFToken=52f09cfb-50e5-47fa-8f69-b7ddaf118db4", driver.getCurrentUrl());
	}

	@Test
	public void registerWithValidDetails() {
		driver.findElement(REGISTERBUTTON).click();

		driver.findElement(By.cssSelector(".regToggle")).click();
		driver.findElement(EMAILADDRESS).clear();
		driver.findElement(EMAILADDRESS).sendKeys("abc_0912@yahoo.com");
		Select titleDropDown = new Select(driver.findElement(By.cssSelector("#register_title")));
		titleDropDown.selectByValue("mrs");
		driver.findElement(FIRSTNAME).clear();
		driver.findElement(FIRSTNAME).sendKeys("Disha");
		driver.findElement(LASTNAME).clear();
		driver.findElement(LASTNAME).sendKeys("Chandel");
		driver.findElement(NEWPASSWORD).clear();
		driver.findElement(NEWPASSWORD).sendKeys("test123");
		driver.findElement(CONFIRMPWD).clear();
		driver.findElement(CONFIRMPWD).sendKeys("test123");

		driver.findElement(By.xpath("//label[@class='ln-c-form-option__label']")).click();
		driver.findElement(SUBMITBUTTON).click();
		Assert.assertEquals("Login | Tu clothing", driver.getTitle());
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/register/success", driver.getCurrentUrl());
	}
		
		
	@Test
	public void registerWithInvalidDetails() {

		driver.findElement(REGISTERBUTTON).click();

		driver.findElement(By.cssSelector(".regToggle")).click();
		driver.findElement(EMAILADDRESS).clear();
		driver.findElement(EMAILADDRESS).sendKeys("abc@0912@yahoo.com");
		Select titleDropDown = new Select(driver.findElement(By.cssSelector("#register_title")));
		titleDropDown.selectByValue("mrs");
		driver.findElement(FIRSTNAME).clear();
		driver.findElement(FIRSTNAME).sendKeys("Disha");
		driver.findElement(LASTNAME).clear();
		driver.findElement(LASTNAME).sendKeys("Chandel");
		driver.findElement(NEWPASSWORD).clear();
		driver.findElement(NEWPASSWORD).sendKeys("test123");
		driver.findElement(CONFIRMPWD).clear();
		driver.findElement(CONFIRMPWD).sendKeys("test123");

		driver.findElement(By.xpath("//label[@class='ln-c-form-option__label']")).click();
		driver.findElement(SUBMITBUTTON).click();
		Assert.assertEquals("Login | Tu clothing", driver.getTitle());
		Assert.assertEquals("Please correct the errors below.", driver.findElement(By.cssSelector("p")).getText());
	}

	@Test
	public void registerWithExistingEmail() {

		driver.findElement(REGISTERBUTTON).click();

		driver.findElement(By.cssSelector(".regToggle")).click();
		driver.findElement(EMAILADDRESS).clear();
		driver.findElement(EMAILADDRESS).sendKeys("disha_0912@yahoo.com");
		Select titleDropDown = new Select(driver.findElement(By.cssSelector("#register_title")));
		titleDropDown.selectByValue("mrs");
		driver.findElement(FIRSTNAME).clear();
		driver.findElement(FIRSTNAME).sendKeys("Disha");
		driver.findElement(LASTNAME).clear();
		driver.findElement(LASTNAME).sendKeys("Chandel");
		driver.findElement(NEWPASSWORD).clear();
		driver.findElement(NEWPASSWORD).sendKeys("test123");
		driver.findElement(CONFIRMPWD).clear();
		driver.findElement(CONFIRMPWD).sendKeys("test123");

		driver.findElement(By.xpath("//label[@class='ln-c-form-option__label']")).click();
		driver.findElement(SUBMITBUTTON).click();
		Assert.assertEquals("Login | Tu clothing", driver.getTitle());
		Assert.assertEquals("You already have a Tu account. Please login to continue.", driver.findElement(By.cssSelector("span")).getText());
		
	}

	@Test
	public void registerWithDifferentPWnConfirmedPW() {

		driver.findElement(REGISTERBUTTON).click();

		driver.findElement(By.cssSelector(".regToggle")).click();
		driver.findElement(EMAILADDRESS).clear();
		driver.findElement(EMAILADDRESS).sendKeys("abc_0912@yahoo.com");
		Select titleDropDown = new Select(driver.findElement(By.cssSelector("#register_title")));
		titleDropDown.selectByValue("mrs");
		driver.findElement(FIRSTNAME).clear();
		driver.findElement(FIRSTNAME).sendKeys("");
		driver.findElement(LASTNAME).clear();
		driver.findElement(LASTNAME).sendKeys("");
		driver.findElement(NEWPASSWORD).clear();
		driver.findElement(NEWPASSWORD).sendKeys("test123");
		driver.findElement(CONFIRMPWD).clear();
		driver.findElement(CONFIRMPWD).sendKeys("test1234");

		driver.findElement(By.xpath("//label[@class='ln-c-form-option__label']")).click();
		driver.findElement(SUBMITBUTTON).click();
		Assert.assertEquals("Login | Tu clothing", driver.getTitle());
		Assert.assertEquals("Password and password confirmation do not match", driver.findElement(By.cssSelector("span")).getText());
		
	}

	@Test
	public void registerWithManadatoryFieldsNotFilled() {

		driver.findElement(REGISTERBUTTON).click();

		driver.findElement(By.cssSelector(".regToggle")).click();
		driver.findElement(EMAILADDRESS).clear();
		driver.findElement(EMAILADDRESS).sendKeys("abc_0912@yahoo.com");
		Select titleDropDown = new Select(driver.findElement(By.cssSelector("#register_title")));
		titleDropDown.selectByValue("mrs");
		driver.findElement(FIRSTNAME).clear();
		driver.findElement(FIRSTNAME).sendKeys("Disha");
		driver.findElement(LASTNAME).clear();
		driver.findElement(LASTNAME).sendKeys("Singh");
		driver.findElement(NEWPASSWORD).clear();
		driver.findElement(NEWPASSWORD).sendKeys("test123");
		driver.findElement(CONFIRMPWD).clear();
		driver.findElement(CONFIRMPWD).sendKeys("test123");

		driver.findElement(By.xpath("//label[@class='ln-c-form-option__label']")).click();
		driver.findElement(SUBMITBUTTON).click();
	}

	@Test
	public void registerWithInvalidNectarCard() {

		driver.findElement(REGISTERBUTTON).click();

		driver.findElement(By.cssSelector(".regToggle")).click();
		driver.findElement(EMAILADDRESS).clear();
		driver.findElement(EMAILADDRESS).sendKeys("abc_0912@yahoo.com");
		Select titleDropDown = new Select(driver.findElement(By.cssSelector("#register_title")));
		titleDropDown.selectByValue("mrs");
		driver.findElement(FIRSTNAME).clear();
		driver.findElement(FIRSTNAME).sendKeys("Disha");
		driver.findElement(LASTNAME).clear();
		driver.findElement(LASTNAME).sendKeys("Singh");
		driver.findElement(NEWPASSWORD).clear();
		driver.findElement(NEWPASSWORD).sendKeys("test123");
		driver.findElement(CONFIRMPWD).clear();
		driver.findElement(CONFIRMPWD).sendKeys("test123");

		driver.findElement(By.xpath("//label[@class='ln-c-form-option__label']")).click();
		driver.findElement(By.cssSelector("#regNectarPointsOne")).clear();
		driver.findElement(By.cssSelector("#regNectarPointsOne")).sendKeys("1234556");
		driver.findElement(By.cssSelector("#regNectarPointsTwo")).clear();
		driver.findElement(By.cssSelector("#regNectarPointsTwo")).sendKeys("556");
		driver.findElement(SUBMITBUTTON).click();
		Assert.assertEquals("Login | Tu clothing", driver.getTitle());
		Assert.assertEquals("Please correct the errors below.", driver.findElement(By.cssSelector("p")).getText());
	}

	@Test
	public void loginWithBlackFields() {
		driver.findElement(LOGIN).click();
		driver.findElement(USERNAME).clear();
		driver.findElement(USERNAME).sendKeys("");
		driver.findElement(PASSWORD).clear();
		driver.findElement(PASSWORD).sendKeys("");
		driver.findElement(LOGINBUTTON).click();
		Assert.assertEquals("Login | Tu clothing", driver.getTitle());
		
		

	}

	@Test
	public void loginWithValidDetails() {
		driver.findElement(LOGIN).click();
		driver.findElement(USERNAME).clear();
		driver.findElement(USERNAME).sendKeys("disha_0912@yahoo.com");
		driver.findElement(PASSWORD).clear();
		driver.findElement(PASSWORD).sendKeys("abc123");
		driver.findElement(LOGINBUTTON).click();
		Assert.assertEquals("Login | Tu clothing", driver.getTitle());
		Assert.assertEquals("Womens, Mens, Kids & Baby Fashion  | Tu clothing", driver.getTitle());
	}

	@Test
	public void loginWithInvalidDetails() {
		driver.findElement(LOGIN).click();
		driver.findElement(USERNAME).clear();
		driver.findElement(USERNAME).sendKeys("disha@0912@yahoo.com");
		driver.findElement(PASSWORD).clear();
		driver.findElement(PASSWORD).sendKeys("abc1234");
		driver.findElement(LOGINBUTTON).click();
		Assert.assertEquals("Login | Tu clothing", driver.getTitle());
		Assert.assertEquals("Please check the fields below and try again", driver.findElement(By.cssSelector("p")).getText());

	}

	@Test
	public void loginWithValidUsernameInvalidPwd() {
		driver.findElement(LOGIN).click();
		driver.findElement(USERNAME).clear();
		driver.findElement(USERNAME).sendKeys("disha_0912@yahoo.com");
		driver.findElement(PASSWORD).clear();
		driver.findElement(PASSWORD).sendKeys("abc123345");
		driver.findElement(LOGINBUTTON).click();
		Assert.assertEquals("Login | Tu clothing", driver.getTitle());
		Assert.assertEquals("Please check the fields below and try again", driver.findElement(By.cssSelector("p")).getText());

	}

	@Test
	public void loginWithInvalidUsernameValidPwd() {
		driver.findElement(LOGIN).click();
		driver.findElement(USERNAME).clear();
		driver.findElement(USERNAME).sendKeys("disha@0912@yahoo.com");
		driver.findElement(PASSWORD).clear();
		driver.findElement(PASSWORD).sendKeys("abc123");
		driver.findElement(LOGINBUTTON).click();
		Assert.assertEquals("Login | Tu clothing", driver.getTitle());
		Assert.assertEquals("Please check the fields below and try again", driver.findElement(By.cssSelector("p")).getText());

	}

	@Test
	public void loginWithForgotPassword() {
		driver.findElement(LOGIN).click();
		driver.findElement(USERNAME).clear();
		driver.findElement(USERNAME).sendKeys("disha_0912@yahoo.com");

		driver.findElement(By.cssSelector("#forgot-password-link")).click();
		Assert.assertEquals("Login | Tu clothing", driver.getTitle());
		Assert.assertEquals("Forgotten Tu password", driver.findElement(By.cssSelector("h2")).getText());
		
	}

	@Test
	public void addToCartWithOneItem() {
		
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("jeans");
		driver.findElement(SEARCHBUTTON).click();

		driver.findElement(By.cssSelector(
				"img[data-src='https://dxs8u2q9547g0.cloudfront.net/sys-master/products/hfa/hbd/11824911155230/136451396_Red_01.jpg_LISTER']"))
				.click();

		driver.findElement(By.cssSelector("div[data-value='136451492']")).click();

		Select quantityDropDown = new Select(
				driver.findElement(By.cssSelector("select[data-testid='productVariantQty']")));
		quantityDropDown.selectByValue("1");

		driver.findElement(By.cssSelector("#AddToCart")).click();
		
		Assert.assertEquals("Search results for: jeans | Tu clothing", driver.getTitle());
		Assert.assertEquals("Womens Pink Washed Mom Jeans | Tu clothing", driver.getTitle());
		
		

	}

	@Test
	public void addToCartWithMulitpleItems() {
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("jeans");
		driver.findElement(SEARCHBUTTON).click();

		driver.findElement(By.cssSelector(
				"img[data-src='https://dxs8u2q9547g0.cloudfront.net/sys-master/products/hfa/hbd/11824911155230/136451396_Red_01.jpg_LISTER']"))
				.click();

		driver.findElement(By.cssSelector("div[data-value='136451492']")).click();

		Select quantityDropDown = new Select(
				driver.findElement(By.cssSelector("select[data-testid='productVariantQty']")));
		quantityDropDown.selectByValue("1");

		driver.findElement(By.cssSelector("#AddToCart")).click();

		// Item 2

		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("tops");
		driver.findElement(SEARCHBUTTON).click();
		driver.findElement(By.cssSelector("label[for='Women-d']")).click();

		driver.findElement(By.cssSelector("img[alt='Dark Green Top With Linen']")).click();

		driver.findElement(By.cssSelector("div[data-value='136549289']")).click();

		Select quantityDropDown2 = new Select(
				driver.findElement(By.cssSelector("select[data-testid='productVariantQty']")));
		quantityDropDown2.selectByValue("1");
		driver.findElement(By.cssSelector("#AddToCart")).click();
		Assert.assertEquals("Search results for: jeans | Tu clothing", driver.getTitle());

		Assert.assertEquals("Womens Pink Washed Mom Jeans | Tu clothing", driver.getTitle());
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=tops", driver.getCurrentUrl());
		
		

	}

	@Test
	public void checkoutForExistingCustomerCnC() {
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("jeans");
		driver.findElement(SEARCHBUTTON).click();

		driver.findElement(By.cssSelector(
				"img[data-src='https://dxs8u2q9547g0.cloudfront.net/sys-master/products/hfa/hbd/11824911155230/136451396_Red_01.jpg_LISTER']"))
				.click();

		driver.findElement(By.cssSelector("div[data-value='136451505']")).click();

		Select quantityDropDown = new Select(
				driver.findElement(By.cssSelector("select[data-testid='productVariantQty']")));
		quantityDropDown.selectByValue("1");

		driver.findElement(By.cssSelector("#AddToCart")).click();
		driver.findElement(CHECKOUTBUTTON).click();
		driver.findElement(CHECKOUTBUTTONBOTTOM).click();
		driver.findElement(USERNAME).clear();
		driver.findElement(USERNAME).sendKeys("disha_0912@yahoo.com");
		driver.findElement(PASSWORD).clear();
		driver.findElement(PASSWORD).sendKeys("disha_0912@yahoo.com");
		driver.findElement(LOGINBUTTON).click();
		driver.findElement(By.cssSelector("label[for='CLICK_AND_COLLECT'")).click();
		driver.findElement(By.cssSelector("input[type='submit']")).click();
		driver.findElement(By.cssSelector("#lookup")).clear();
		driver.findElement(By.cssSelector("#lookup")).sendKeys("NW9 9BP");
		driver.findElement(By.cssSelector("button[aria-label='Look up']")).click();
		
		Assert.assertEquals("Search results for: jeans | Tu clothing", driver.getTitle());
		Assert.assertEquals("Womens Pink Washed Mom Jeans | Tu clothing", driver.getTitle());
		

	}

	@Test
	public void checkoutForGuestCustomerHomeDelivery() {

		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("jeans");
		driver.findElement(SEARCHBUTTON).click();

		driver.findElement(By.cssSelector(
				"img[data-src='https://dxs8u2q9547g0.cloudfront.net/sys-master/products/hfa/hbd/11824911155230/136451396_Red_01.jpg_LISTER']"))
				.click();

		driver.findElement(By.cssSelector("div[data-value='136451505']")).click();

		Select quantityDropDown = new Select(
				driver.findElement(By.cssSelector("select[data-testid='productVariantQty']")));
		quantityDropDown.selectByValue("1");

		driver.findElement(By.cssSelector("#AddToCart")).click();
		driver.findElement(CHECKOUTBUTTON).click();
		driver.findElement(CHECKOUTBUTTONBOTTOM).click();
		driver.findElement(By.cssSelector("#guest_email")).clear();
		driver.findElement(By.cssSelector("#guest_email")).sendKeys("abc_912@yahoo.com");
		driver.findElement(By.cssSelector(".HOME_DELIVERY")).click();
		driver.findElement(By.cssSelector("input[type='submit']")).click();
		driver.findElement(By.cssSelector("#lookup")).clear();
		driver.findElement(By.cssSelector("#lookup")).sendKeys("NW9 9BP");
		driver.findElement(By.cssSelector("button[aria-label='Look up']")).click();
		Assert.assertEquals("Search results for: jeans | Tu clothing", driver.getTitle());
		Assert.assertEquals("Womens Pink Washed Mom Jeans | Tu clothing", driver.getTitle());
		Assert.assertEquals("Your shopping bag | Tu clothing", driver.getTitle());
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/login/checkout", driver.getCurrentUrl());
		Assert.assertEquals("Checkout | Tu clothing", driver.getTitle());
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/home-delivery/add", driver.getCurrentUrl());
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/home-delivery/select-home-delivery-mode", driver.getCurrentUrl());
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/order-summary/view", driver.getCurrentUrl());
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/payment-method/add", driver.getCurrentUrl());
		
	}
@Test
public void checkoutForGuestCustomerClicknCollect() {
	driver.findElement(SEARCHTEXTBOX).clear();
	driver.findElement(SEARCHTEXTBOX).sendKeys("jeans");
	driver.findElement(SEARCHBUTTON).click();
	
	driver.findElement(By.cssSelector(
			"img[data-src='https://dxs8u2q9547g0.cloudfront.net/sys-master/products/hfa/hbd/11824911155230/136451396_Red_01.jpg_LISTER']"))
			.click();
	
	driver.findElement(By.cssSelector("div[data-value='136451505']")).click();

	
	Select quantityDropDown = new Select(
			driver.findElement(By.cssSelector("select[data-testid='productVariantQty']")));
	quantityDropDown.selectByValue("1");

	driver.findElement(By.cssSelector("#AddToCart")).click();
	driver.findElement(CHECKOUTBUTTON).click();
	driver.findElement(CHECKOUTBUTTONBOTTOM).click();
	driver.findElement(By.cssSelector("#guest_email")).clear();
	driver.findElement(By.cssSelector("#guest_email")).sendKeys("abc_0912@yahoo.com");
	driver.findElement(By.cssSelector("label[for='CLICK_AND_COLLECT'")).click();	
	driver.findElement(By.cssSelector("input[type='submit']")).click();
	driver.findElement(By.cssSelector("#lookup")).clear();
	driver.findElement(By.cssSelector("#lookup")).sendKeys("NW9 9BP");
	driver.findElement(By.cssSelector("button[aria-label='Look up']")).click();
	Assert.assertEquals("Search results for: jeans | Tu clothing", driver.getTitle());
	Assert.assertEquals("Womens Pink Washed Mom Jeans | Tu clothing", driver.getTitle());
	Assert.assertEquals("Your shopping bag | Tu clothing", driver.getTitle());
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/login/checkout", driver.getCurrentUrl());
	Assert.assertEquals("Checkout | Tu clothing", driver.getTitle());
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/pickup-location/findStore", driver.getCurrentUrl());
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/order-summary/view", driver.getCurrentUrl());
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/payment-method/add", driver.getCurrentUrl());
}


	@After
	public void close() {
		driver.close();
	}
}