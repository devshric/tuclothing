
public class UnitConversions {

	public static void main(String[] args) {

DistanceConversions distanceConversions = new DistanceConversions();
distanceConversions.kmtom();
distanceConversions.mtokm();
distanceConversions.mtocm();
distanceConversions.cmtom();

WeightConversions weightConversions = new WeightConversions();
weightConversions.KgtoG();
weightConversions.GtoKg();
weightConversions.GtoMg();
weightConversions.MgtoG();

VolumeConversions volumeConversions= new VolumeConversions();
volumeConversions.LtoMl();
volumeConversions.MltoL();

TempConversions tempConversions = new TempConversions();

tempConversions.FtoC();
tempConversions.CtoF();
	}
}
	




