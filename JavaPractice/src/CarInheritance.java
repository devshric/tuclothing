
public class CarInheritance {

	void breaks() {
		System.out.println("This car has power breaks");
	}

	void wheels() {
		System.out.println("This car has 4 wheels");
	}

	void doors() {
		System.out.println("This car has 5 doors");
	}

}