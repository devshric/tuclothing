
public class StudentReportTestData {

	int english = 90, maths = 99, science = 93, history = 78, computer = 97, hindi = 80;
	int total = english + maths + science + history + computer + hindi;
	double average = total / 6;

	void StudentTotal() {
		System.out.println("English=" + english);
		System.out.println("Maths=" + maths);
		System.out.println("Science=" + science);
		System.out.println("History=" + history);
		System.out.println("Computer=" + computer);
		System.out.println("Hindi=" + hindi);
		System.out.println("Total of all subjects=" + total);
	}

	void StudentAverage() {

		System.out.println("Average of all subjects=" + average);

	}

	void StudentGrade() {
		if (average <= 35) {
			System.out.println("You have failed!!!!");
		} else if (average >= 36 && average <= 49) {
			System.out.println("You have passed in third grade.");
		} else if (average >= 50 && average <= 59) {
			System.out.println("You have passed in second grade.");

		} else if (average >= 60 && average <= 74) {
			System.out.println("You have passed in first grade.");
		}

		else if (average >= 75) {
			System.out.println("**Congratulations!! You have passed with distinction.**");
		}
	}
}
