package AddToCartFunctionality;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class AddToCartWithMultipleItems {
	public static By SEARCHTEXTBOX = By.cssSelector("#search");
	public static By SEARCHBUTTON = By.cssSelector(".searchButton");

	public static void main(String[] args) {
	
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\CEX\\Documents\\DevshriClasses\\Automation\\chromedriver.exe");
		
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get("https://tuclothing.sainsburys.co.uk/");
		
		
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("jeans");
		driver.findElement(SEARCHBUTTON).click();		

		driver.findElement(By.cssSelector("img[data-src='https://dxs8u2q9547g0.cloudfront.net/sys-master/products/hfa/hbd/11824911155230/136451396_Red_01.jpg_LISTER']")).click();
	
		driver.findElement(By.cssSelector("div[data-value='136451505']")).click();
		
		
		Select quantityDropDown = new Select(driver.findElement(By.cssSelector("select[data-testid='productVariantQty']")));
		quantityDropDown.selectByValue("1");
		
		
		driver.findElement(By.cssSelector("#AddToCart")).click();
		
		//Item 2
		
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("tops");
		driver.findElement(SEARCHBUTTON).click();	
		driver.findElement(By.cssSelector("label[for='Women-d']")).click();
	
		driver.findElement(By.cssSelector("img[alt='Dark Green Top With Linen']")).click();
		
		driver.findElement(By.cssSelector("div[data-value='136549289']")).click();
		
		Select quantityDropDown2 = new Select(driver.findElement(By.cssSelector("select[data-testid='productVariantQty']")));
		quantityDropDown2.selectByValue("1");
		driver.findElement(By.cssSelector("#AddToCart")).click();
		
		
	}

}
