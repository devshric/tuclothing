package RegisterCSSSelector;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class RegisterWithExistingEmail {
	public static By REGISTERBUTTON = By.cssSelector("a[href='/login']");
	public static By EMAILADDRESS = By.cssSelector("#register_email");
	public static By FIRSTNAME = By.cssSelector("#register_firstName");
	public static By LASTNAME = By.cssSelector("#register_lastName");
	public static By NEWPASSWORD = By.cssSelector("#password");
	public static By CONFIRMPWD = By.cssSelector("#register_checkPwd");
	public static By SUBMITBUTTON = By.cssSelector("#submit-register");

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\CEX\\Documents\\DevshriClasses\\Automation\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		driver.get("https://tuclothing.sainsburys.co.uk/");
		driver.findElement(REGISTERBUTTON).click();

		driver.findElement(By.cssSelector(".regToggle")).click();
		driver.findElement(EMAILADDRESS).clear();
		driver.findElement(EMAILADDRESS).sendKeys("disha_0912@yahoo.com");
		Select titleDropDown = new Select(driver.findElement(By.cssSelector("#register_title")));
		titleDropDown.selectByValue("mrs");
		driver.findElement(FIRSTNAME).clear();
		driver.findElement(FIRSTNAME).sendKeys("Disha");
		driver.findElement(LASTNAME).clear();
		driver.findElement(LASTNAME).sendKeys("Chandel");
		driver.findElement(NEWPASSWORD).clear();
		driver.findElement(NEWPASSWORD).sendKeys("test123");
		driver.findElement(CONFIRMPWD).clear();
		driver.findElement(CONFIRMPWD).sendKeys("test123");

		driver.findElement(By.xpath("//label[@class='ln-c-form-option__label']")).click();
		driver.findElement(SUBMITBUTTON).click();

	}


}
