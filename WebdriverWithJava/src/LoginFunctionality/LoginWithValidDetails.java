package LoginFunctionality;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginWithValidDetails {

	public static By LOGIN = By.cssSelector("a[href='/login']");
	public static By USERNAME = By.cssSelector("#j_username");
	public static By PASSWORD = By.cssSelector("#j_password");
	public static By LOGINBUTTON =By.cssSelector("#submit-login");
	
	public static void main(String[] args) {
	
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\CEX\\Documents\\DevshriClasses\\Automation\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		driver.get("https://tuclothing.sainsburys.co.uk/");
		
		//driver.findElement(By.linkText("Tu Log In / Register")).click();
		driver.findElement(LOGIN).click();
		
		//driver.findElement(By.id("j_username")).clear();
		driver.findElement(USERNAME).clear();
		
		driver.findElement(USERNAME).sendKeys("disha_0912@yahoo.com");
		driver.findElement(PASSWORD).clear();
		driver.findElement(PASSWORD).sendKeys("abc123");
		driver.findElement(LOGINBUTTON).click();


	}

}
