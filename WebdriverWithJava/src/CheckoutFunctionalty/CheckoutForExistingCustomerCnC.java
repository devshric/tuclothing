package CheckoutFunctionalty;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CheckoutForExistingCustomerCnC {
	public static By SEARCHTEXTBOX = By.cssSelector("#search");
	public static By SEARCHBUTTON = By.cssSelector(".searchButton");
	public static By CHECKOUTBUTTON =By.cssSelector("a[data-testid='checkoutButton']");
	public static By CHECKOUTBUTTONBOTTOM = By.cssSelector("#basketButtonBottom");
	public static By USERNAME = By.cssSelector("#j_username");
	public static By PASSWORD = By.cssSelector("#j_password");
	public static By LOGINBUTTON =By.cssSelector("#submit-login");
	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\CEX\\Documents\\DevshriClasses\\Automation\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();

		driver.get("https://tuclothing.sainsburys.co.uk/");

		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("jeans");
		driver.findElement(SEARCHBUTTON).click();
		
		driver.findElement(By.cssSelector(
				"img[data-src='https://dxs8u2q9547g0.cloudfront.net/sys-master/products/hfa/hbd/11824911155230/136451396_Red_01.jpg_LISTER']"))
				.click();
		
		driver.findElement(By.cssSelector("div[data-value='136451505']")).click();

		
		Select quantityDropDown = new Select(
				driver.findElement(By.cssSelector("select[data-testid='productVariantQty']")));
		quantityDropDown.selectByValue("1");

		driver.findElement(By.cssSelector("#AddToCart")).click();
		driver.findElement(CHECKOUTBUTTON).click();
		driver.findElement(CHECKOUTBUTTONBOTTOM).click();
		driver.findElement(USERNAME).clear();
		driver.findElement(USERNAME).sendKeys("disha_0912@yahoo.com");
		driver.findElement(PASSWORD).clear();
		driver.findElement(PASSWORD).sendKeys("disha_0912@yahoo.com");
		driver.findElement(LOGINBUTTON).click();
		driver.findElement(By.cssSelector("label[for='CLICK_AND_COLLECT'")).click();	
		driver.findElement(By.cssSelector("input[type='submit']")).click();
		driver.findElement(By.cssSelector("#lookup")).clear();
		driver.findElement(By.cssSelector("#lookup")).sendKeys("NW9 9BP");
		driver.findElement(By.cssSelector("button[aria-label='Look up']")).click();
		
}

}
