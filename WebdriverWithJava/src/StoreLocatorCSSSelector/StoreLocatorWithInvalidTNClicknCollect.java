package StoreLocatorCSSSelector;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class StoreLocatorWithInvalidTNClicknCollect {
	
	public static By STORELOCATOR = By.cssSelector("a[href='/store-finder']");
	public static By POSTCODEORTOWN = By.cssSelector("input[placeholder='Postcode or town']");
	public static By FINDSTOREBUTTON = By.cssSelector("button[class='ln-c-button ln-c-button--primary']");

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\CEX\\Documents\\DevshriClasses\\Automation\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		driver.get("https://tuclothing.sainsburys.co.uk/");

		driver.findElement(STORELOCATOR).click();

		driver.findElement(POSTCODEORTOWN).clear();
		driver.findElement(POSTCODEORTOWN).sendKeys("hsdkjahd");
		driver.findElement(By.cssSelector("label[for='children']")).click();
		driver.findElement(By.cssSelector("label[for='click']")).click();

		driver.findElement(FINDSTOREBUTTON).click();

}
}
