package SearchFunctionality;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SearchWithInvalidDeptName {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\CEX\\Documents\\DevshriClasses\\Automation\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		driver.get("https://tuclothing.sainsburys.co.uk/");
		driver.findElement(By.id("search")).clear();
		driver.findElement(By.id("search")).sendKeys("hkjhhk");
		driver.findElement(By.className("searchButton")).click();


	}

}
