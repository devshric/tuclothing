package SearchCSSSelector;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SearchWithBlank {
	
	public static By SEARCHTEXTBOX = By.cssSelector("#search");
	public static By SEARCHBUTTON = By.cssSelector(".searchButton");

	public static void main(String[] args) {
	
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\CEX\\Documents\\DevshriClasses\\Automation\\chromedriver.exe");
		
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://tuclothing.sainsburys.co.uk/");
		
		
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("");
		driver.findElement(SEARCHBUTTON).click();
		
	}

 

	

}
